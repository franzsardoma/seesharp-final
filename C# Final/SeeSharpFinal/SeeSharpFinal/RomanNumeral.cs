﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SeeSharpFinal
{
    public class RomanNumeral : INumber
    {
        String RomanValue;
        int intValue = 0;
        public bool isValid = true;
        public RomanNumeral(String Roman)
        {
            
            this.RomanValue = Roman;
            Parse();
            if(isValid == true)
            {
                this.intValue = RomantoInteger(RomanValue);
            }
            Console.WriteLine();
        }

        public int toInteger()
        {
            if (isValid == false)
            {
                Console.WriteLine("Roman Numeral to Integer: -1");
               // intValue = -1;
            }
            else
            {
                Console.WriteLine("Roman Numeral to Integer: {0}", intValue);
            }
            
            return intValue;
        }

        public string toRomanNumeral()
        {
            if (isValid == false)
            {
                //Console.WriteLine("Integer Value to Roman Numeral: INVALID");
                RomanValue = "INVALID";
            }
            else
            {
                RomanValue = toRomanNumeral(intValue);
         
            }
            Console.WriteLine("Roman Numeral Value: " + RomanValue);
            return RomanValue;
        }

        public string toStringNumber()
        {
            if (isValid == false)
            {
                Console.WriteLine("Roman Numeral to String Value: INVALID");
            }
            else
            {
                Console.WriteLine("Roman Numeral to String Value: ");
            }
            return null;
        }

        public bool Parse()
        {

            //   List<string> list = new List<string>() { "CM", "CD", "D", "XC", "C", "XL", "L", "IX", "X", "IV", "V", "I" };
            //  // string RomanValue = "XI";
            //   for (int i = 0; i < list.Count; i++)
            //   {
            //       if (RomanValue.Contains(list[i]))
            //       {
            //           RomanValue = RomanValue.Replace(list[i], "");
            //       }
            //   }

            ////var result = RomanValue.Equals(""); /*? true : false;*/
            //   if (RomanValue.Equals(""))
            //   {
            //       isValid = false;
            //   }
            //   else
            //   {
            //       if (intValue > 1000 || intValue < 0)
            //       {
            //           isValid = false;
            //           return isValid;
            //       }
            //   }


            //   return isValid;

            int M = 0, D = 0, C = 0, L = 0, X = 0, V = 0, I = 0, invalid = 0;

            for (int x = 0; x < RomanValue.Length; x++)
            {
                if (RomanValue[x] == 'M')
                {
                    M++;
                }
                else if (RomanValue[x] == 'D')
                {
                    D++;
                }
                else if (RomanValue[x] == 'C')
                {
                    C++;
                }
                else if (RomanValue[x] == 'L')
                {
                    L++;
                }
                else if (RomanValue[x] == 'X')
                {
                    X++;
                }
                else if (RomanValue[x] == 'V')
                {
                    V++;
                }
                else if (RomanValue[x] == 'I')
                {
                    I++;
                }
                else
                {
                    invalid++;
                }
            }

            if (I >= 4 || X >= 5 || V >= 2 || L >= 4 || D >= 4 || M >= 2 || invalid != 0 || RomanValue.Equals(""))
            {
                isValid = false;
            }
            else
            {
                isValid = true;
            }

            return isValid;

        }

        int ConvertedToInteger()
        {
            return RomantoInteger(RomanValue);
        }

        //Integer to roman numeral
        public static string toRomanNumeral(int numValue)
        {
                string roman = "";
                List<int> romanIntValueList = new List<int>() { 1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1 };
                List<String> romanValueList = new List<String>() { "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I" };
                for (int x = 0; x < romanIntValueList.Count; x++)
                {
                    if (numValue >= romanIntValueList[x])
                    {
                        roman += romanValueList[x];
                        numValue -= romanIntValueList[x];
                        x = 0;
                    }
                }
            return roman;
        }

        // Convert Roman Numeral to Integer
        public int RomantoInteger(string romanVal)
        {
            
            Dictionary<char, int> RomanDict = new Dictionary<char, int>()
            {
                {'I', 1},
                {'V', 5},
                {'X', 10},
                {'L', 50},
                {'C', 100},
                {'D', 500},
                {'M', 1000}
            };

            int number = 0;
            for (int x = 0; x < romanVal.Length; x++)
            {
                if (x + 1 < romanVal.Length && RomanDict[romanVal[x]] < RomanDict[romanVal[x + 1]])
                {
                    number -= RomanDict[romanVal[x]];
                }
                else
                {
                    number += RomanDict[romanVal[x]];
                }
            }
            return number;
        }

        public static RomanNumeral operator +(RomanNumeral firstNumber, RomanNumeral secondNumber)
        {
            return new RomanNumeral(toRomanNumeral(firstNumber.ConvertedToInteger() + secondNumber.ConvertedToInteger()));
        }
        public static RomanNumeral operator -(RomanNumeral firstNumber, RomanNumeral secondNumber)
        {
            return new RomanNumeral(toRomanNumeral(firstNumber.ConvertedToInteger() - secondNumber.ConvertedToInteger()));
        }
        public static RomanNumeral operator /(RomanNumeral firstNumber, RomanNumeral secondNumber)
        {
            return new RomanNumeral(toRomanNumeral(firstNumber.ConvertedToInteger() / secondNumber.ConvertedToInteger()));
        }
        public static RomanNumeral operator *(RomanNumeral firstNumber, RomanNumeral secondNumber)
        {
            return new RomanNumeral(toRomanNumeral(firstNumber.ConvertedToInteger() * secondNumber.ConvertedToInteger()));
        }
        public static RomanNumeral operator %(RomanNumeral firstNumber, RomanNumeral secondNumber)
        {
            return new RomanNumeral(toRomanNumeral(firstNumber.ConvertedToInteger() % secondNumber.ConvertedToInteger()));
        }

        public static bool operator >(RomanNumeral firstNumber, RomanNumeral secondNumber)
        {
            
            if (firstNumber.intValue > secondNumber.intValue)
            {
                return true;
            }
            return false;
        }

        public static bool operator <(RomanNumeral firstNumber, RomanNumeral secondNumber)
        {
            if (firstNumber.intValue < secondNumber.intValue)
            {
                return true;
            }
            return false;
        }
        public static bool operator >=(RomanNumeral firstNumber, RomanNumeral secondNumber)
        {
            if (firstNumber.intValue >= secondNumber.intValue)
            {
                return true;
            }
            return false;
        }
        public static bool operator <=(RomanNumeral firstNumber, RomanNumeral secondNumber)
        {
            if (firstNumber.intValue <= secondNumber.intValue)
            {
                return true;
            }
            return false;
        }
        public static bool operator ==(RomanNumeral firstNumber, RomanNumeral secondNumber)
        {
            if (firstNumber.intValue == secondNumber.intValue)
            {
                return true;
            }
            return false;
        }
        public static bool operator !=(RomanNumeral firstNumber, RomanNumeral secondNumber)
        {
            if (firstNumber.intValue != secondNumber.intValue)
            {
                return true;
            }
            else
            {
                return false;
            }

        }


    

    }
}
