﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeeSharpFinal
{
    public interface INumber
    {
        int toInteger();
        string toRomanNumeral();
        string toStringNumber();
        bool Parse();
    }
}
