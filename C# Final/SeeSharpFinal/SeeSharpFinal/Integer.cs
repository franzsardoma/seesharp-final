﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeeSharpFinal
{
    public class Integer : INumber
    {
        public int intValue;
        public string RomanValue;
        public bool isValid = true;

        public Integer(int number)
        {
            this.intValue = number;
            Parse();
            
            Console.WriteLine();
        }

        public int toInteger()
        {
            if (isValid == false)
            {
                Console.WriteLine("Integer Value: -1");
                intValue = 10000;
            }
            else
            {
                Console.WriteLine("Integer Value: {0}", intValue);
            }
            return intValue;
        }
        public string toRomanNumeral()
        {
            if (isValid == false)
            {
                Console.WriteLine("Integer Value to Roman Numeral: INVALID");
                RomanValue = "";
            }
            else
            {
                RomanValue = toRomanNumeral(intValue);
                Console.WriteLine("Integer Value to Roman Numeral: " + RomanValue);
            }
            
            return RomanValue;


        }
        public string toStringNumber()
        {
            if (isValid == false)
            {
                Console.WriteLine("Integer to String Value: INVALID");
            }
            else
            {
                Console.WriteLine("Integer to String Value: ");
            }

            return "";
        }

        public bool Parse()
        {
            if (intValue > 1000 || intValue <= 0)
            {
                isValid = false;
                return isValid;
            }

            return isValid;
        }



        public string toRomanNumeral(int numValue)
        {

            string roman = "";
            List<int> testList =  new List<int>(){ 1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1 };
            List<String> testList2 = new List<String>() { "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I" };


            for (int x = 0; x < testList.Count; x++)
            {
                if (numValue >= testList[x])
                {
                    roman += testList2[x];
                    numValue -= testList[x];
                    x = 0;
                }
            }
            return roman;
        }
        public static Integer operator +(Integer FirstNumb, Integer SecondNumb)
        {
            return new Integer(FirstNumb.intValue + SecondNumb.intValue);
        }

        public static Integer operator -(Integer FirstNumb, Integer SecondNumb)
        {
            return new Integer(FirstNumb.intValue - SecondNumb.intValue);
        }

        public static Integer operator *(Integer FirstNumb, Integer SecondNumb)
        {
            return new Integer(FirstNumb.intValue * SecondNumb.intValue);
        }

        public static Integer operator /(Integer FirstNumb, Integer SecondNumb)
        {
            return new Integer(FirstNumb.intValue / SecondNumb.intValue);
        }

        public static Integer operator %(Integer FirstNumb, Integer SecondNumb)
        {
            return new Integer(FirstNumb.intValue % SecondNumb.intValue);
        }
        public static bool operator <(Integer FirstNumb, Integer SecondNumb)
        {
            if (FirstNumb.intValue < SecondNumb.intValue)
            {
                return true;
            }
            return false;
        }

        public static bool operator >(Integer FirstNumb, Integer SecondNumb)
        {
            if (FirstNumb.intValue > SecondNumb.intValue)
            {
                return true;
            }
            return false;
        }

        public static bool operator >=(Integer FirstNumb, Integer SecondNumb)
        {
            if (FirstNumb.intValue >= SecondNumb.intValue)
            {
                return true;
            }
            return false;
        }
        public static bool operator <=(Integer FirstNumb, Integer SecondNumb)
        {
            if (FirstNumb.intValue <= SecondNumb.intValue)
            {
                return true;
            }
            return false;
        }
        public static bool operator ==(Integer FirstNumb, Integer SecondNumb)
        {
            if (FirstNumb.intValue == SecondNumb.intValue)
            {
                return true;
            }
            return false;
        }
        public static bool operator !=(Integer FirstNumb, Integer SecondNumb)
        {
            if (FirstNumb.intValue != SecondNumb.intValue)
            {
                return true;
            }
            return false;
        }
    }
}
